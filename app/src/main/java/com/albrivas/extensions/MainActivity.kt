package com.albrivas.extensions

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.android.material.button.MaterialButton
import kotlinx.android.synthetic.main.activity_main.* // Extension utilizada

class MainActivity : AppCompatActivity(){

    private lateinit var btnFind: MaterialButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        instancias()
        acciones()
    }

    private fun acciones() {
        // Boton previamente instanciado con findViewById
        btnFind.setOnClickListener { sendMessage("Boton findViewById pulsado") }

        // Boton utilizado directamente gracias a la extension
        buttonExtension.setOnClickListener { sendMessage("Boton Extension pulsado") }
    }

    private fun instancias() {
        btnFind = findViewById(R.id.buttonFind)
    }

    private fun sendMessage(message: String) {
        Toast.makeText(this,  message, Toast.LENGTH_SHORT).show()
    }
}
